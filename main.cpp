#include <iostream>
#include <vector>

using namespace std;

int main()
{
    vector<int> list;
    while (true)
    {
        int n;
        cin >> n;

        if (n == 0)
            break;

        list.push_back(n);
    }

    for (int i = 0; i < list.size(); i++)
    {
        int number = list.at(i);
        cout << number << endl;
    }
    return 0;
}